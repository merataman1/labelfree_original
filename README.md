# Simple data analysis workflow
This workflow handles a complete proteomics data analysis. The input are raw LC-MS files and the output are protein reports. 

The main steps are: 

1.  Create a philosopher workspace
2.  Download a database
3.  Search with MSFragger
4.  PeptideProphet
5.  ProteinProphet
6.  Filter with FDR
7.  Quantify with labelfree
8.  Report
9.  Run ionquant
10. MSStats
11. Generate data from string db

More details can be found [here](https://github.com/Nesvilab/philosopher/wiki/Simple-Data-Analysis).

## Usage
Usage with the `proteomics` tool.

```
proteomics local \
           --name test01 \
           --ram 30G \
           --output_dir /out/ \
           --input_dir /data/ \
           --species_id 10090 \
           --workflow labelfree
```
   
## Pipeline
The pipeline runs a small labelfree workflow and tests for certain output files. Take a look at
`.gitlab-ci.yml` to learn how that exactly works.

